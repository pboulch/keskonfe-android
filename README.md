# Keskonfé Android

<p float="left">
  <img src="https://play-lh.googleusercontent.com/ZdcrpPE9mjhqCbSkBo5YKqOgE_poBssFMuKpVdNuxm77L7yPmUO69mdhTlUmA_tIcIQ=w5120-h2880" width="300" />
  <img src="https://play-lh.googleusercontent.com/vnNVEGZ-QQIFXzzUoWMxwfn8XBuVsg8gKCx05drXtLi_qknXDVCXK8bDiii3CxeTEA=w5120-h2880" width="300" /> 
  <img src="https://play-lh.googleusercontent.com/OvSnEH0N8ezGWUH5b2NtofHz9yhru6kbtej3uvtFgEDrURWBUvK2gLFmn1990XYK1g=w5120-h2880" width="300" />
</p>



**Keskonfé application agenda d'événènements culturels**

## Le projet
L'idée est de proposer aux habitants d'une ville, une application regroupant tous les évènements culturels autour d'eux, notamment ceux organisés par des associations et des bars.


## Fonctionnalités 
* Récuparation d'une liste d'évènements sur une instance Mobilizon
* Affichage de la liste d'évènements
* Mise en place d'un moteur de recherche, basé sur la recherche Mobilizon
* Affichage d'une page détail de l'évènement

## Roadmap
* Ajouter la possibilité pour l'utilisateur de gérer un profil : s'abonner à certains organisateur (réception des notifications lorsque nouveaux évènements ... )
* Mettre en place les pages des organisations 
* Ajout de filtre supplémentaire (évènement gratuit / payant par exemple)
* Ajout de nouvelles villes

