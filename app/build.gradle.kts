plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("com.google.devtools.ksp") version "1.8.10-1.0.9"
    id("kotlin-parcelize")
    id("com.apollographql.apollo3") version "3.7.3"
}

android {
    compileSdk = 34

    defaultConfig {
        applicationId = "app.keskonfe.android"
        minSdk = 23
        targetSdk = 34
        versionCode = 4
        versionName = "0.2.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }

    kotlinOptions {
        jvmTarget = "17"
    }


    buildFeatures {
        compose = true
    }

    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }

    applicationVariants.all {
        kotlin.sourceSets {
            getByName(name) {
                kotlin.srcDir("build/generated/ksp/${name}/kotlin")
            }
        }
    }

    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    namespace = "app.keskonfe.android"
}

dependencies {
    implementation("androidx.core:core-ktx:1.13.1")

    implementation("androidx.security:security-crypto-ktx:1.1.0-alpha06")
    implementation("androidx.datastore:datastore-preferences:1.1.1")

    val composeBom = platform("androidx.compose:compose-bom:2024.06.00")

    implementation(composeBom)
    implementation("androidx.compose.ui:ui")
    implementation("androidx.compose.ui:ui-text")
    implementation("androidx.compose.material:material")
    implementation("androidx.compose.ui:ui-tooling-preview")
    implementation("androidx.compose.runtime:runtime-livedata")
    implementation("androidx.compose.ui:ui-util")

    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.8.2")
    implementation("androidx.activity:activity-compose:1.9.0")

    implementation("com.squareup.retrofit2:converter-gson:2.9.0")
    implementation("com.squareup.retrofit2:retrofit:2.9.0")

    implementation("io.insert-koin:koin-android:3.5.0")
    implementation("io.insert-koin:koin-androidx-compose:3.5.0")

    implementation("io.coil-kt:coil-compose:2.4.0")

    implementation("com.google.android.material:material:1.12.0")

    implementation("com.jakewharton.timber:timber:5.0.1")

    implementation("io.arrow-kt:arrow-core:1.0.1")

    implementation("com.apollographql.apollo3:apollo-runtime:3.7.3")

    implementation("androidx.palette:palette-ktx:1.0.0")

    implementation("de.charlex.compose:html-text:1.5.0")

    implementation("androidx.constraintlayout:constraintlayout-compose:1.1.0-alpha13")
    implementation("com.google.accompanist:accompanist-flowlayout:0.32.0")
    implementation("com.google.accompanist:accompanist-permissions:0.32.0")
    implementation("com.google.accompanist:accompanist-systemuicontroller:0.32.0")
    implementation("com.google.accompanist:accompanist-webview:0.33.2-alpha")

    implementation("com.airbnb.android:lottie-compose:6.1.0")

    implementation("io.github.raamcosta.compose-destinations:animations-core:1.9.53")
    implementation("androidx.browser:browser:1.8.0")

    coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:2.0.4")


    ksp("io.github.raamcosta.compose-destinations:ksp:1.9.53")


    testImplementation("junit:junit:4.13.2")

    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation("androidx.compose.ui:ui-test-junit4:1.5.2")

    debugImplementation("androidx.compose.ui:ui-tooling")
}

apollo {
    service("service") {
        packageName.set("app.keskonfe.android")
        mapScalarToKotlinString("UUID")
    }
}