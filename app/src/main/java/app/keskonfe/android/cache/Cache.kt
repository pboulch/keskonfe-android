package app.keskonfe.android.cache

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async

interface Cache<Key : Any, Value : Any> {
    fun getAsync(key: Key): Deferred<Value?>
    fun setAsync(key: Key, value: Value): Deferred<Unit>

    fun compose(b: Cache<Key, Value>): Cache<Key, Value> {
        return object : Cache<Key, Value> {
            override fun getAsync(key: Key): Deferred<Value?> {
                return CoroutineScope(Dispatchers.Default).async {
                    this@Cache.getAsync(key).await() ?: let {
                        b.getAsync(key).await()?.apply {
                            this@Cache.setAsync(key, this).await()
                        }
                    }
                }
            }

            override fun setAsync(key: Key, value: Value): Deferred<Unit> {
                return CoroutineScope(Dispatchers.Default).async {
                    listOf(
                        this@Cache.setAsync(key, value),
                        b.setAsync(key, value)
                    ).forEach { it.await() }
                }
            }
        }
    }

    fun reuseInflight(): Cache<Key, Value> {
        return object : Cache<Key, Value> {
            val map = mutableMapOf<Key, Deferred<Value?>>()

            override fun getAsync(key: Key): Deferred<Value?> {
                return map[key] ?: this@Cache.getAsync(key).apply {
                    map[key] = this

                    CoroutineScope(Dispatchers.Default).async {
                        join()
                        map.remove(key)
                    }
                }
            }

            override fun setAsync(key: Key, value: Value): Deferred<Unit> {
                return this@Cache.setAsync(key, value)
            }
        }
    }
}