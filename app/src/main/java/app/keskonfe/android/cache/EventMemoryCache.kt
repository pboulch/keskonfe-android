package app.keskonfe.android.cache

import app.keskonfe.android.models.Event
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import java.util.UUID

class EventMemoryCache : Cache<UUID, Event> {
    private val memoryCache: HashMap<UUID, Event> = hashMapOf()

    override fun getAsync(key: UUID): Deferred<Event?> {
        return CoroutineScope(Dispatchers.IO).async {
            memoryCache[key]
        }
    }

    override fun setAsync(key: UUID, value: Event): Deferred<Unit> {
        return CoroutineScope(Dispatchers.Default).async {
            memoryCache[key] = value
        }
    }
}