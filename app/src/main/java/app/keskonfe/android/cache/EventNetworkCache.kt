package app.keskonfe.android.cache

import app.keskonfe.android.EventQuery
import app.keskonfe.android.di.apolloClient
import app.keskonfe.android.models.Event
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import timber.log.Timber
import java.util.UUID

class EventNetworkCache() : Cache<UUID, Event> {
    override fun getAsync(key: UUID): Deferred<Event?> {
        return CoroutineScope(Dispatchers.IO).async {
            try {

                var query = EventQuery(uuid = key.toString())
                val response = apolloClient.query(query).execute()
                if (response.data != null && response.data!!.event != null) {
                    Event(response.data!!.event!!)
                }
                Timber.d("fetchFirstEvents Success ${response.data}")

            } catch (e: Exception) {
                Timber.e("fetchFirstEvents error ${e.localizedMessage}")
            }
            null
        }
    }

    override fun setAsync(key: UUID, value: Event): Deferred<Unit> {
        return CompletableDeferred(Unit)
    }
}