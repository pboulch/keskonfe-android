package app.keskonfe.android.data

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.datastore.preferences.core.emptyPreferences
import app.keskonfe.android.models.City
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException

const val CITY_NAME = "city_name"

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
    name = CITY_NAME
)

class FeedDataStore(preference_datastore: DataStore<Preferences>) {
    private val CITY_NAME = stringPreferencesKey("city_name")

    val cityNameFlow: Flow<City?> = preference_datastore.data
        .catch {
            if (it is IOException) {
                it.printStackTrace()
                emit(emptyPreferences())
            } else {
                throw it
            }
        }
        .map { preferences ->
            preferences[CITY_NAME]?.let { City.valueOf(it) }
        }

    suspend fun saveCityToFeedDataStore(cityName: City, context: Context) {
        context.dataStore.edit { preferences ->
            preferences[CITY_NAME] = cityName.toString()
        }
    }
}