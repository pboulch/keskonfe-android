package app.keskonfe.android.di

import com.apollographql.apollo3.ApolloClient

val apolloClient = ApolloClient.Builder()
    .serverUrl("https://events.keskonfe.app/graphiql")
    .build()