package app.keskonfe.android.di

import app.keskonfe.android.cache.EventMemoryCache
import app.keskonfe.android.cache.EventNetworkCache
import app.keskonfe.android.data.FeedDataStore
import app.keskonfe.android.data.dataStore
import app.keskonfe.android.repository.EventRepository
import app.keskonfe.android.repository.EventRepositoryImpl
import app.keskonfe.android.repository.FeedRepository
import app.keskonfe.android.repository.FeedRepositoryImpl
import app.keskonfe.android.vm.events.EventDetailViewModel
import app.keskonfe.android.vm.mainfeed.MainFeedViewModel
import app.keskonfe.android.vm.onboarding.OnboardingViewModel
import kotlinx.coroutines.Dispatchers
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val appModule = module {

    single(named("Background")) { Dispatchers.Default }

    single<FeedRepository> {
        FeedRepositoryImpl(
            get(named("Background")),
            get(),
            get()
        )
    }

    single<EventRepository> {
        EventRepositoryImpl(
            get(named("Background")),
            get()
        )
    }

    single {
        /*
          Crée un cache qui va d'abord regarder si l'offre est en mémoire.
          Si ce n'est pas le cas, elle est chargée via l'API unitairement.
          Si un chargement pour l'offre demandée est déjà en cours, on en relance pas un
          deuxième, on attend que le premier se termine.
         */
        EventMemoryCache().compose(EventNetworkCache().reuseInflight())
    }

    single<FeedDataStore> { FeedDataStore(androidContext().dataStore) }

    viewModel { MainFeedViewModel(get(), get()) }
    viewModel { params -> EventDetailViewModel(params.get(), get()) }
    viewModel { OnboardingViewModel(get()) }

}
