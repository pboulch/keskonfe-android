package app.keskonfe.android.models

import app.keskonfe.android.EventListQuery
import app.keskonfe.android.EventQuery
import app.keskonfe.android.SearchEventsQuery

data class Actor(
    val name: String,
    val avatar: String
) {
    constructor(dto: EventListQuery.OrganizerActor?) : this(
        name = dto?.name ?: "",
        avatar = dto?.avatar?.url ?: "",
    )

    constructor(dto: SearchEventsQuery.OrganizerActor?) : this(
        name = dto?.name ?: "",
        avatar = dto?.avatar?.url ?: "",
    )

    constructor(dto: EventQuery.OrganizerActor?) : this(
        name = dto?.name ?: "",
        avatar = dto?.avatar?.url ?: "",
    )
}