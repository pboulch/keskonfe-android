package app.keskonfe.android.models

import app.keskonfe.android.EventListQuery
import app.keskonfe.android.EventQuery
import app.keskonfe.android.SearchEventsQuery

data class Address(
    val id: String,
    val description: String,
    val locality: String,
    val street: String,
    val geom: String
) {
    constructor(dto: EventListQuery.PhysicalAddress?) : this(
        id = dto?.id ?: "",
        locality = dto?.locality ?: "",
        description = dto?.description ?: "",
        street = dto?.street ?: "",
        geom = dto?.geom.toString()
    )

    constructor(dto: SearchEventsQuery.PhysicalAddress?) : this(
        id = dto?.id ?: "",
        locality = dto?.locality ?: "",
        description = dto?.description ?: "",
        street = dto?.street ?: "",
        geom = dto?.geom.toString()
    )

    constructor(dto: EventQuery.PhysicalAddress?) : this(
        id = dto?.id ?: "",
        locality = dto?.locality ?: "",
        description = dto?.description ?: "",
        street = dto?.street ?: "",
        geom = dto?.geom.toString()
    )
}