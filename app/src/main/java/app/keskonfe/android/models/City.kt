package app.keskonfe.android.models

enum class City(val locCode: String) {
    DEFAULT(locCode = ""),
    TOULOUSE(locCode = "spc00cgxs822"),
    RENNES(locCode = "u0918y7sv")
}