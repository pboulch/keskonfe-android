package app.keskonfe.android.models

import android.annotation.SuppressLint
import app.keskonfe.android.EventListQuery
import app.keskonfe.android.EventQuery
import app.keskonfe.android.SearchEventsQuery
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

data class Event(
    val uuid: UUID,
    val title: String,
    val description: String,
    val url: String,
    val picture: String,
    val organizer: Actor,
    val physicalAddress: Address,
    val category: EventCategory,
    var beginsOn: LocalDateTime,
    var externalParticipationUrl: String?
) {
    @SuppressLint("SimpleDateFormat")
    constructor(dto: EventListQuery.Element) : this(
        uuid = UUID.fromString((dto.uuid ?: "") as String),
        title = dto.title ?: "",
        description = dto.description ?: "",
        url = dto.url ?: "",
        picture = dto.picture?.url ?: "",
        physicalAddress = Address(dto.physicalAddress),
        category = EventCategory.fromDto(dto.category),
        beginsOn = LocalDateTime.parse(
            dto.beginsOn as CharSequence?,
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
        ),
        organizer = Actor(dto = dto.organizerActor),
        externalParticipationUrl = null
    )


    constructor(dto: SearchEventsQuery.Element) : this(
        uuid = UUID.fromString((dto.uuid ?: "") as String),
        title = dto.title ?: "",
        description = "",
        url = dto.url ?: "",
        picture = dto.picture?.url ?: "",
        physicalAddress = Address(dto.physicalAddress),
        category = EventCategory.fromDto(dto.category),
        beginsOn = LocalDateTime.parse(
            dto.beginsOn as CharSequence?,
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
        ),
        organizer = Actor(dto = dto.organizerActor), //Actor(dto = dto.organizerActor)
        externalParticipationUrl = null
    )

    constructor(dto: EventQuery.Event) : this(
        uuid = UUID.fromString((dto.uuid ?: "") as String),
        title = dto.title ?: "",
        description = dto.description ?: "",
        url = dto.url ?: "",
        picture = dto.picture?.url ?: "",
        physicalAddress = Address(dto.physicalAddress),
        category = EventCategory.fromDto(dto.category),
        beginsOn = LocalDateTime.parse(
            dto.beginsOn as CharSequence?,
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
        ),
        organizer = Actor(dto = dto.organizerActor), //Actor(dto = dto.organizerActor)
        externalParticipationUrl = dto.onlineAddress
    )

}