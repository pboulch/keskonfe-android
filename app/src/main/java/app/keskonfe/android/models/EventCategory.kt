package app.keskonfe.android.models

import app.keskonfe.android.R

enum class EventCategory(val labelId: Int) {
    ARTS(R.string.event_category_arts),
    AUTO_BOAT_AIR(R.string.event_category_auto_boat_air),
    BOOK_CLUBS(R.string.event_category_book_club),
    BUSINESS(R.string.event_category_business),
    CAUSES(R.string.event_category_causes),
    COMEDY(R.string.event_category_comedy),
    COMMUNITY(R.string.event_category_community),
    CRAFTS(R.string.event_category_crafts),
    FAMILY_EDUCATION(R.string.event_category_family_education),
    FASHION_BEAUTY(R.string.event_category_fashion_beauty),
    FILM_MEDIA(R.string.event_category_film_media),
    FOOD_DRINK(R.string.event_category_food_drink),
    GAMES(R.string.event_category_games),
    HEALTH(R.string.event_category_health),
    LANGUAGE_CULTURE(R.string.event_category_language_culture),
    LEARNING(R.string.event_category_learning),
    LGBTQ(R.string.event_category_LGBTQ),
    MEETING(R.string.event_category_meeting),
    MOVEMENTS_POLITICS(R.string.event_category_movements_politics),
    MUSIC(R.string.event_category_music),
    NETWORKING(R.string.event_category_networking),
    OUTDOORS_ADVENTURE(R.string.event_category_outdoors_adventure),
    PARTY(R.string.event_category_party),
    PERFORMING_VISUAL_ARTS(R.string.event_category_performing_visual_arts),
    PETS(R.string.event_category_pets),
    PHOTOGRAPHY(R.string.event_category_photography),
    SCIENCE_TECH(R.string.event_category_science_tech),
    SPIRITUALITY_RELIGION_BELIEFS(R.string.event_category_spirituality_religion_beliefs),
    SPORTS(R.string.event_category_sports),
    THEATRE(R.string.event_category_theatre),
    UNKNOWN(R.string.event_category_unknown);

    companion object {

        fun fromDto(dto: app.keskonfe.android.type.EventCategory?): EventCategory {
            return when (dto) {
                app.keskonfe.android.type.EventCategory.ARTS -> ARTS
                app.keskonfe.android.type.EventCategory.AUTO_BOAT_AIR -> AUTO_BOAT_AIR
                app.keskonfe.android.type.EventCategory.BOOK_CLUBS -> BOOK_CLUBS
                app.keskonfe.android.type.EventCategory.BUSINESS -> BUSINESS
                app.keskonfe.android.type.EventCategory.CAUSES -> CAUSES
                app.keskonfe.android.type.EventCategory.COMEDY -> COMEDY
                app.keskonfe.android.type.EventCategory.COMMUNITY -> COMMUNITY
                app.keskonfe.android.type.EventCategory.CRAFTS -> CRAFTS
                app.keskonfe.android.type.EventCategory.FAMILY_EDUCATION -> FAMILY_EDUCATION
                app.keskonfe.android.type.EventCategory.FASHION_BEAUTY -> FASHION_BEAUTY
                app.keskonfe.android.type.EventCategory.FILM_MEDIA -> FILM_MEDIA
                app.keskonfe.android.type.EventCategory.FOOD_DRINK -> FOOD_DRINK
                app.keskonfe.android.type.EventCategory.GAMES -> GAMES
                app.keskonfe.android.type.EventCategory.HEALTH -> HEALTH
                app.keskonfe.android.type.EventCategory.LANGUAGE_CULTURE -> LANGUAGE_CULTURE
                app.keskonfe.android.type.EventCategory.LEARNING -> LEARNING
                app.keskonfe.android.type.EventCategory.LGBTQ -> LGBTQ
                app.keskonfe.android.type.EventCategory.MEETING -> MEETING
                app.keskonfe.android.type.EventCategory.MOVEMENTS_POLITICS -> MOVEMENTS_POLITICS
                app.keskonfe.android.type.EventCategory.MUSIC -> MUSIC
                app.keskonfe.android.type.EventCategory.NETWORKING -> NETWORKING
                app.keskonfe.android.type.EventCategory.OUTDOORS_ADVENTURE -> OUTDOORS_ADVENTURE
                app.keskonfe.android.type.EventCategory.PARTY -> PARTY
                app.keskonfe.android.type.EventCategory.PERFORMING_VISUAL_ARTS -> PERFORMING_VISUAL_ARTS
                app.keskonfe.android.type.EventCategory.PETS -> PETS
                app.keskonfe.android.type.EventCategory.PHOTOGRAPHY -> PHOTOGRAPHY
                app.keskonfe.android.type.EventCategory.SCIENCE_TECH -> SCIENCE_TECH
                app.keskonfe.android.type.EventCategory.SPIRITUALITY_RELIGION_BELIEFS -> SPIRITUALITY_RELIGION_BELIEFS
                app.keskonfe.android.type.EventCategory.SPORTS -> SPORTS
                app.keskonfe.android.type.EventCategory.THEATRE -> THEATRE
                app.keskonfe.android.type.EventCategory.UNKNOWN__ -> UNKNOWN
                else -> {
                    UNKNOWN
                }
            }
        }
    }
}