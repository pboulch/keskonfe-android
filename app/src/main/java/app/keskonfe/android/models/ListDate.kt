package app.keskonfe.android.models

enum class ListDate {
    TODAY,
    TOMORROW,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY,
    NEXT_WEEK,
    THIS_MONTH,
    LATER
}