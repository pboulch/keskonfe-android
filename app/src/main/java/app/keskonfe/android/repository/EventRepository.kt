package app.keskonfe.android.repository

import app.keskonfe.android.EventQuery
import app.keskonfe.android.cache.Cache
import app.keskonfe.android.di.apolloClient
import app.keskonfe.android.models.Event
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.UUID

interface EventRepository {
    suspend fun fetchEvent(uuid: UUID)
    fun getCurrentEvent(): StateFlow<Event?>
}

class EventRepositoryImpl(
    private val dispatcher: CoroutineDispatcher,
    private val cache: Cache<UUID, Event>
) : EventRepository {

    private val currentEvent = MutableStateFlow<Event?>(null)

    override fun getCurrentEvent() = currentEvent.asStateFlow()

    override suspend fun fetchEvent(uuid: UUID) {
        val event = cache.getAsync(uuid).await()
        currentEvent.update { event }
        if (event?.description?.isEmpty() == true) {
            var query = EventQuery(uuid = uuid.toString())
            val response = apolloClient.query(query).execute()
            if (response.data != null && response.data!!.event != null) {
                val event = Event(response.data!!.event!!)
                addEventToCache(event)
                currentEvent.update { event }
            }
        }
    }

    private fun addEventToCache(event: Event) {
        CoroutineScope(Dispatchers.Default).launch {
            try {
                cache.setAsync(event.uuid, event).await()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

}