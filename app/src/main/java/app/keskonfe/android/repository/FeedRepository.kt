package app.keskonfe.android.repository

import android.content.Context
import androidx.compose.runtime.collectAsState
import app.keskonfe.android.SearchEventsQuery
import app.keskonfe.android.cache.Cache
import app.keskonfe.android.data.FeedDataStore
import app.keskonfe.android.di.apolloClient
import app.keskonfe.android.models.City
import app.keskonfe.android.models.Event
import app.keskonfe.android.models.EventCategory
import app.keskonfe.android.models.ListDate
import com.apollographql.apollo3.api.Optional
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.Calendar
import java.util.Date
import java.util.UUID

const val EVENTS_PAGE_SIZE = 20 // TODO put that in some file.properties in resources

interface FeedRepository {

    fun getMainFeed(): StateFlow<Map<ListDate,List<Event>>>
    fun getCurrentCategory(): StateFlow<EventCategory>
    fun getCurrentSearch(): StateFlow<String?>
    fun getCity(): Flow<City?>

    fun isLoading(): StateFlow<Boolean>

    suspend fun fetchFeed()
    suspend fun loadMore()

    fun updateSearch(term: String?)
    suspend fun saveCityToFeedDataStore(city: City, context: Context)
}

class FeedRepositoryImpl(
    private val dispatcher: CoroutineDispatcher,
    private val eventCache: Cache<UUID, Event>,
    private val feedDataStore: FeedDataStore
) : FeedRepository {

    private var currentCategoryFlow = MutableStateFlow(EventCategory.UNKNOWN)
    private val mainFeedFlow = MutableStateFlow<Map<ListDate,List<Event>>>(mapOf())
    private val currentSearchFlow = MutableStateFlow<String?>(null)
    private val isLoadingFlow = MutableStateFlow(false)

    private val city = feedDataStore.cityNameFlow


    override fun getMainFeed() = mainFeedFlow.asStateFlow()
    override fun getCurrentCategory() = currentCategoryFlow.asStateFlow()
    override fun getCurrentSearch() = currentSearchFlow.asStateFlow()
    override fun isLoading() = isLoadingFlow.asStateFlow()
    override fun getCity(): Flow<City?> = feedDataStore.cityNameFlow

    init {
        CoroutineScope(dispatcher).launch {
            fetchFeed()
        }
    }

    override suspend fun fetchFeed() {
        try {
            isLoadingFlow.update { true }
            var query = SearchEventsQuery(
                page = Optional.present(1),
                category = Optional.present(null),
                term = Optional.presentIfNotNull(currentSearchFlow.value),
                location = Optional.present(city.firstOrNull()?.locCode)
            )
            val response = apolloClient.query(query).execute()
            if (response.data != null && response.data!!.searchEvents != null && response.data!!.searchEvents!!.elements != null) {
                mainFeedFlow.update {
                    val listEvents =
                        response.data!!.searchEvents!!.elements.filterNotNull().map { Event(it) }
                    addEventToCache(listEvents)
                    val mapEvent = listEvents.groupBy {
                        it.beginsOn.toLocalDate()
                    }
                    getMapOfListEvents(mapEvent)
                }
            }
            Timber.d("fetchFirstEvents Success ${response.data}")

        } catch (e: Exception) {
            Timber.e("fetchFirstEvents error ${e.localizedMessage}")
        }
        isLoadingFlow.update { false }
    }

    override suspend fun loadMore() {

        if (mainFeedFlow.value.count() % EVENTS_PAGE_SIZE != 0 || mainFeedFlow.value.isEmpty()) {
            return
        }

        isLoadingFlow.update { true }

        val nextPage = (mainFeedFlow.value.size / EVENTS_PAGE_SIZE) + 1

        val response = apolloClient.query(
            SearchEventsQuery(
                page = Optional.present(nextPage),
                category = Optional.present(null),
                term = Optional.presentIfNotNull(currentSearchFlow.value),
                location = Optional.present("spc00cgxs822")
            )
        ).execute()
        if (response.data != null && response.data!!.searchEvents != null && response.data!!.searchEvents!!.elements != null) {
            mainFeedFlow.update { listEvent ->
                val newListEvents =
                    response.data!!.searchEvents!!.elements!!.filterNotNull().map { Event(it) }
                addEventToCache(newListEvents)
                val oldListEvent = listEvent.flatMap { it.value }
                getMapOfListEvents((oldListEvent + newListEvents).groupBy { it.beginsOn.toLocalDate() })
            }
        }

        isLoadingFlow.update { false }
    }

    override fun updateSearch(term: String?) {
        currentSearchFlow.update { term }
    }

    private fun addEventToCache(listEvent: List<Event>) {
        CoroutineScope(Dispatchers.Default).launch {
            listEvent.forEach { event ->
                try {
                    eventCache.setAsync(event.uuid, event).await()
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }
        }
    }

    private fun getMapOfListEvents(mapEvent: Map<LocalDate,List<Event>>): MutableMap<ListDate, List<Event>> {
        var newMap = mutableMapOf<ListDate, List<Event>>()
        mapEvent.keys.forEach { dateOfEvents ->
            when(dateOfEvents) {
                LocalDate.now() -> {
                    mapEvent[dateOfEvents]?.let {
                        newMap[ListDate.TODAY] = it
                    }
                }
                LocalDate.now().plusDays(1) -> {
                    mapEvent[dateOfEvents]?.let {
                        newMap[ListDate.TOMORROW] = it
                    }
                }
                LocalDate.now().with(DayOfWeek.WEDNESDAY) -> {
                    mapEvent[dateOfEvents]?.let {
                        newMap[ListDate.WEDNESDAY] = it
                    }
                }
                LocalDate.now().with(DayOfWeek.THURSDAY) -> {
                    mapEvent[dateOfEvents]?.let {
                        newMap[ListDate.THURSDAY] = it
                    }
                }
                LocalDate.now().with(DayOfWeek.FRIDAY) -> {
                    mapEvent[dateOfEvents]?.let {
                        newMap[ListDate.FRIDAY] = it
                    }
                }
                LocalDate.now().with(DayOfWeek.SATURDAY) -> {
                    mapEvent[dateOfEvents]?.let {
                        newMap[ListDate.SATURDAY] = it
                    }
                }
                LocalDate.now().with(DayOfWeek.SUNDAY) -> {
                    mapEvent[dateOfEvents]?.let {
                        newMap[ListDate.SUNDAY] = it
                    }
                }
                in LocalDate.now().with(DayOfWeek.SUNDAY).plusDays(1) .. LocalDate.now().with(DayOfWeek.SUNDAY).plusDays(7) -> {
                    mapEvent[dateOfEvents]?.let {
                        newMap[ListDate.NEXT_WEEK] = if(newMap[ListDate.NEXT_WEEK] == null) {
                            it
                        } else {
                            newMap[ListDate.NEXT_WEEK]!!+it
                        }
                    }
                }
            }
        }
        return newMap
    }

    override suspend fun saveCityToFeedDataStore(city: City, context: Context) {
        feedDataStore.saveCityToFeedDataStore(cityName = city, context= context)
        fetchFeed()
    }

}