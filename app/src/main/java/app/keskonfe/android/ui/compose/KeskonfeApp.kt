package app.keskonfe.android.ui.compose

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.unit.dp
import androidx.navigation.compose.rememberNavController
import app.keskonfe.android.utils.compose.KBottomSheetHostState
import app.keskonfe.android.utils.compose.rememberKBottomSheetNavigator
import com.google.accompanist.navigation.material.ExperimentalMaterialNavigationApi
import com.google.accompanist.navigation.material.ModalBottomSheetLayout
import com.ramcosta.composedestinations.DestinationsNavHost
import com.ramcosta.composedestinations.animations.rememberAnimatedNavHostEngine
import com.ramcosta.composedestinations.navigation.dependency

@OptIn(
    ExperimentalAnimationApi::class, ExperimentalMaterialNavigationApi::class
)
@Composable
fun KeskonfeApp() {

    val kBottomSheetHostState = remember { KBottomSheetHostState() }
    var bottomSheetNavigator = rememberKBottomSheetNavigator(kBottomSheetHostState)
    val navController = rememberNavController(bottomSheetNavigator)

    ModalBottomSheetLayout(
        bottomSheetNavigator = bottomSheetNavigator,
        //other configuration for you bottom sheet screens, like:
        sheetShape = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp),
    ) {
        DestinationsNavHost(
            navGraph = NavGraphs.home,
            engine = rememberAnimatedNavHostEngine(),
            navController = navController,
            dependenciesContainerBuilder = {
                dependency(kBottomSheetHostState)
            }
        )
    }

}