package app.keskonfe.android.ui.compose.common.buttons

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.unit.dp
import app.keskonfe.android.ui.compose.common.utils.ResponsiveText
import kotlinx.coroutines.delay
import kotlin.math.log10

@Composable
fun KButton(
    modifier: Modifier = Modifier,
    text: String = "",
    iconStart: Painter? = null,
    iconEnd: Painter? = null,
    onClick: () -> Unit = { },
    style: KButtonStyle = KButtonStyle.XL_FIVE,
    isEnabled: Boolean = true,
    isLoading: Boolean = false
) {
    var delayTotal by remember { mutableFloatStateOf(0f) }
    var progressValue by remember { mutableFloatStateOf(0f) }

    val interactionSource = remember { MutableInteractionSource() }
    val isPressed by interactionSource.collectIsPressedAsState()

    LaunchedEffect(isLoading) {
        while (progressValue < 1 && isLoading) {
            delayTotal += 10
            progressValue = (log10(delayTotal / 200) / 2)
            delay(10)
        }

        progressValue = 0f
        delayTotal = 0f
    }

    val shape = remember { CircleShape }

    var buttonModifier = modifier
        .clip(shape = shape)
        .clickable(
            indication = null,
            enabled = isEnabled,
            interactionSource = interactionSource
        ) {
            if (!isLoading) {
                onClick()
            }
        }

    buttonModifier = buttonModifier
        .background(
            color = if (!isEnabled) {
                style.disabledBackgroundColor
            } else if (isPressed || isLoading) {
                style.selectedBackgroundColor
            } else {
                style.backgroundColor
            },
            shape = shape
        )
        .border(
            border = BorderStroke(
                width = 1.dp,
                color = if (!isEnabled) {
                    style.disabledBorderColor
                } else if (isPressed || isLoading) {
                    style.selectedBorderColor
                } else {
                    style.borderColor
                }
            ),
            shape = shape
        )
        .height(IntrinsicSize.Min)

    Box(
        contentAlignment = Alignment.Center,
        modifier = buttonModifier
    ) {
        ProgressBar(
            progress = progressValue,
            color = style.loadingBackgroundColor,
            shape = shape
        )

        Row(
            horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterHorizontally),
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(style.paddingValues)
        ) {
            val textStyle = if (!isEnabled) {
                style.disabledTextStyle
            } else if (isPressed || isLoading) {
                style.selectedTextStyle
            } else {
                style.textStyle
            }

            iconStart?.let {
                Icon(
                    painter = it,
                    contentDescription = null,
                    modifier = Modifier.height(style.iconHeight),
                    tint = textStyle.color
                )
            }

            ResponsiveText(
                text = text,
                textStyle = textStyle,
                modifier = Modifier.height(IntrinsicSize.Min)
            )

            iconEnd?.let {
                Icon(
                    painter = it,
                    contentDescription = null,
                    modifier = Modifier.height(style.iconHeight),
                    tint = textStyle.color
                )
            }
        }
    }
}

@Composable
fun BoxScope.ProgressBar(
    progress: Float,
    color: Color,
    shape: Shape
) {
    Box(
        modifier = Modifier
            .clip(shape)
            .align(Alignment.CenterStart)
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth(progress)
                .fillMaxHeight()
                .background(color = color)
        ) { }
    }
}
