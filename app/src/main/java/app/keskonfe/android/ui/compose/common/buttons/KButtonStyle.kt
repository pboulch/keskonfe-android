package app.keskonfe.android.ui.compose.common.buttons

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import app.keskonfe.android.ui.theme.ColorFive
import app.keskonfe.android.ui.theme.ColorFour
import app.keskonfe.android.ui.theme.GreyLightColor
import app.keskonfe.android.ui.theme.OpenSansWhite14

enum class KButtonStyle(
    val backgroundColor: Color,
    val selectedBackgroundColor: Color,
    val disabledBackgroundColor: Color,
    val loadingBackgroundColor: Color,
    val textStyle: TextStyle,
    val selectedTextStyle: TextStyle,
    val disabledTextStyle: TextStyle,
    val iconHeight: Dp,
    val borderColor: Color,
    val selectedBorderColor: Color,
    val disabledBorderColor: Color,
    val paddingValues: PaddingValues
) {
    XL_FIVE(
        backgroundColor = ColorFive,
        selectedBackgroundColor = ColorFour,
        disabledBackgroundColor = GreyLightColor,
        loadingBackgroundColor = ColorFour,
        textStyle = OpenSansWhite14,
        selectedTextStyle = OpenSansWhite14,
        disabledTextStyle = OpenSansWhite14,
        iconHeight = 16.dp,
        borderColor = Color.Transparent,
        selectedBorderColor = Color.Transparent,
        disabledBorderColor = Color.Transparent,
        paddingValues = PaddingValues(horizontal = 32.dp, vertical = 16.dp)
    ),

    XL_TRANSPARENT(
        backgroundColor = Color.Transparent,
        selectedBackgroundColor = ColorFive,
        disabledBackgroundColor = GreyLightColor,
        loadingBackgroundColor = ColorFive,
        textStyle = OpenSansWhite14,
        selectedTextStyle = OpenSansWhite14,
        disabledTextStyle = OpenSansWhite14,
        iconHeight = 16.dp,
        borderColor = Color.White,
        selectedBorderColor = Color.Transparent,
        disabledBorderColor = Color.Transparent,
        paddingValues = PaddingValues(horizontal = 32.dp, vertical = 16.dp)
    ),

    L_FIVE(
        backgroundColor = ColorFive,
        selectedBackgroundColor = ColorFour,
        disabledBackgroundColor = GreyLightColor,
        loadingBackgroundColor = ColorFour,
        textStyle = OpenSansWhite14,
        selectedTextStyle = OpenSansWhite14,
        disabledTextStyle = OpenSansWhite14,
        iconHeight = 16.dp,
        borderColor = Color.Transparent,
        selectedBorderColor = Color.Transparent,
        disabledBorderColor = Color.Transparent,
        paddingValues = PaddingValues(horizontal = 16.dp, vertical = 8.dp)
    ),

}
