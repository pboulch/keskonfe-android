package app.keskonfe.android.ui.compose.common.fields

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import app.keskonfe.android.R
import app.keskonfe.android.ui.compose.common.utils.clearFocusOnKeyboardDismiss
import app.keskonfe.android.ui.theme.OpenSansWhite14


@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun KTextBox(
    value: String,
    hint: String,
    focus: Boolean = false,
    onFocusChange: (isFocused: Boolean) -> Unit = { },
    onValueChange: (value: String?) -> Unit = { },
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    focusRequester: FocusRequester = FocusRequester(),
) {
    var isFocused by remember { mutableStateOf(focus) }

    var currentSelection by remember {
        mutableStateOf(
            TextFieldValue(
                value,
                TextRange(value.length)
            )
        )
    }


    // If value is set to "", it will be saved in the local state
    // we should probably review all that ...
    LaunchedEffect(value) {
        if (value.isBlank()) {
            currentSelection = TextFieldValue("")
        }
    }

    BasicTextField(
        modifier = Modifier
            .fillMaxWidth()
            .height(48.dp)
            .border(
                width = 1.dp,
                color = White,
                shape = RoundedCornerShape(30.dp)
            )
            .background(Transparent, shape = RoundedCornerShape(8.dp))
            .padding(horizontal = 12.dp)
            .focusRequester(focusRequester)
            .onFocusChanged {
                isFocused = it.isFocused
                onFocusChange(it.isFocused)
            }
            .clearFocusOnKeyboardDismiss(),
        value = currentSelection,
        onValueChange = { s ->
            if (!s.text.contains("\n")) {
                onValueChange(s.text)
                currentSelection = s
            }
        },
        singleLine = true,
        textStyle = if (isFocused) OpenSansWhite14 else OpenSansWhite14,
        keyboardOptions = keyboardOptions,
        keyboardActions = keyboardActions,
        decorationBox = { innerTextField ->
            Row(
                horizontalArrangement = Arrangement.spacedBy(4.dp),
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                Box(modifier = Modifier.weight(1f)) {
                    if (currentSelection.text.isEmpty()) {
                        Text(
                            text = hint,
                            style = OpenSansWhite14,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis
                        )
                    }

                    innerTextField()
                }

                if (currentSelection.text.isNotEmpty()) {
                    IconButton(
                        onClick = {
                            onValueChange(currentSelection.text)
                        },
                        modifier = Modifier
                            .align(alignment = Alignment.CenterVertically)
                            .size(24.dp)
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_search),
                            contentDescription = stringResource(id = R.string.common_erase),
                            tint = White,
                            modifier = Modifier
                                .align(Alignment.CenterVertically)
                                .size(12.dp)
                        )
                    }
                    IconButton(
                        onClick = {
                            currentSelection = TextFieldValue("")
                            onValueChange(null)
                        },
                        modifier = Modifier
                            .align(alignment = Alignment.CenterVertically)
                            .size(24.dp)
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_cross),
                            contentDescription = stringResource(id = R.string.common_erase),
                            tint = White,
                            modifier = Modifier
                                .align(Alignment.CenterVertically)
                                .size(12.dp)
                        )
                    }
                }
            }
        }
    )

    if (isFocused) {
        LaunchedEffect(Unit) {
            focusRequester.requestFocus()
        }
    }
}