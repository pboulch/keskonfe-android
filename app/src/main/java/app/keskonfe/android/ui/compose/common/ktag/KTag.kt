package app.keskonfe.android.ui.compose.common.ktag

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.keskonfe.android.extensions.silentClickable
import app.keskonfe.android.ui.theme.ColorFive
import app.keskonfe.android.ui.theme.NotoFamily

enum class KTagStyle(
    val backgroundColor: Color,
    val textColor: Color,
    val borderColor: Color,
    val fontSize: TextUnit,
    val horizontalPadding: Dp,
    val verticalPadding: Dp
) {
    CARD(
        backgroundColor = Color.Transparent,
        textColor = ColorFive,
        borderColor = ColorFive,
        fontSize = 12.sp,
        horizontalPadding = 8.dp,
        verticalPadding = 4.dp
    ),

    ONBOARDING(
        backgroundColor = Color.Transparent,
        textColor = Color.White,
        borderColor = Color.White,
        fontSize = 18.sp,
        horizontalPadding = 16.dp,
        verticalPadding = 8.dp
    ),

    SHEET(
        backgroundColor = Color.Transparent,
        textColor = ColorFive,
        borderColor = ColorFive,
        fontSize = 18.sp,
        horizontalPadding = 16.dp,
        verticalPadding = 8.dp
    )
}


@Composable
fun KTag(content: String, style: KTagStyle, isSelected: Boolean, onTap: (() -> Unit)? = null) {

    val borderColor = if (isSelected) {
        ColorFive
    } else {
        Color.Transparent
    }

    Box(
        modifier = Modifier
            .clip(
                RoundedCornerShape(100.dp)
            )
            .background(borderColor)
    ) {
        Box(modifier = Modifier.padding(4.dp)) {
            KTagContent(content = content, style = style, isSelected = isSelected) {
                onTap?.invoke()
            }
        }

    }

}

@Composable
private fun KTagContent(
    content: String,
    style: KTagStyle,
    isSelected: Boolean,
    onTap: (() -> Unit)?
) {
    Box(
        modifier = Modifier.border(
            width = 1.dp,
            color = style.borderColor,
            shape = RoundedCornerShape(50.dp)
        )
    ) {
        Text(text = content,
            style = if (isSelected) {
                TextStyle(
                    fontFamily = NotoFamily,
                    fontWeight = FontWeight.SemiBold,
                    color = style.textColor,
                    fontSize = style.fontSize
                )
            } else {
                TextStyle(
                    fontFamily = NotoFamily,
                    fontWeight = FontWeight.Normal,
                    color = style.textColor,
                    fontSize = style.fontSize
                )
            },
            modifier = Modifier
                .clip(
                    RoundedCornerShape(50.dp)
                )
                .background(style.backgroundColor)
                .padding(horizontal = style.horizontalPadding, vertical = style.verticalPadding)
                .silentClickable {
                    onTap?.invoke()
                }
        )
    }

}