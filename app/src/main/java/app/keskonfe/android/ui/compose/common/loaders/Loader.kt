package app.keskonfe.android.ui.compose.common.loaders

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import app.keskonfe.android.R
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition

@Composable
fun Loader() {
    val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.loader))
    LottieAnimation(
        composition,
        iterations = LottieConstants.IterateForever
    )
}