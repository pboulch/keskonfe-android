package app.keskonfe.android.ui.compose.common.topbar

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import app.keskonfe.android.ui.theme.GreyBorderColor
import kotlin.math.roundToInt

@Composable
fun TopBarView(
    modifier: Modifier = Modifier,
    viewLeft: (@Composable BoxScope.() -> Unit)? = null,
    viewCenter: (@Composable BoxScope.() -> Unit)? = null,
    viewRight: (@Composable BoxScope.() -> Unit)? = null,
    withDivider: Boolean = false
) {
    Column(modifier = modifier.fillMaxWidth()) {
        Row(
            horizontalArrangement = spaceBetween3Responsively,
            modifier = Modifier
                .height(48.dp)
                .fillMaxWidth()
        ) {
            Box(modifier = Modifier.align(Alignment.CenterVertically)) {
                if (viewLeft != null) {
                    viewLeft()
                }
            }
            Box(
                modifier = Modifier
                    .weight(1f, fill = false)
                    .align(Alignment.CenterVertically)
            ) {
                if (viewCenter != null) {
                    viewCenter()
                }
            }
            Box(modifier = Modifier.align(Alignment.CenterVertically)) {
                if (viewRight != null) {
                    viewRight()
                }
            }
        }

        AnimatedVisibility(visible = withDivider) {
            Divider(color = GreyBorderColor, thickness = 1.dp)
        }
    }
}

private val spaceBetween3Responsively = object : Arrangement.HorizontalOrVertical {
    override val spacing = 0.dp

    override fun Density.arrange(
        totalSize: Int,
        sizes: IntArray,
        layoutDirection: LayoutDirection,
        outPositions: IntArray,
    ) = if (layoutDirection == LayoutDirection.Ltr) {
        placeResponsivelyBetween(totalSize, sizes, outPositions, reverseInput = false)
    } else {
        placeResponsivelyBetween(totalSize, sizes, outPositions, reverseInput = true)
    }

    override fun Density.arrange(
        totalSize: Int,
        sizes: IntArray,
        outPositions: IntArray,
    ) = placeResponsivelyBetween(totalSize, sizes, outPositions, reverseInput = false)

    override fun toString() = "Arrangement#SpaceBetween3Responsively"
}

private fun placeResponsivelyBetween(
    totalSize: Int,
    size: IntArray,
    outPosition: IntArray,
    reverseInput: Boolean,
) {
    val gapSizes = calculateGapSize(totalSize, size)

    val sizesArray = if (reverseInput) {
        size.reversedArray()
    } else {
        size
    }

    var current = 0f
    sizesArray.forEachIndexed { index, it ->
        outPosition[index] = current.roundToInt()

        // here the element and gap placement happens
        current += it.toFloat() + gapSizes[index]
    }
}

private fun calculateGapSize(totalSize: Int, itemSizes: IntArray): List<Int> {
    return if (itemSizes.sum() == totalSize) { // the items take up the whole space and there's no space for any gaps
        listOf(0, 0, 0)
    } else {
        val startOf2ndIfInMiddle = totalSize / 2 - itemSizes[1] / 2

        val firstGap = Integer.max(startOf2ndIfInMiddle - itemSizes.first(), 0)
        val secondGap = totalSize - itemSizes.sum() - firstGap

        listOf(firstGap, secondGap, 0)
    }
}