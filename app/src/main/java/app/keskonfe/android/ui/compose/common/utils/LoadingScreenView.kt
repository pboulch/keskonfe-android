package app.keskonfe.android.ui.compose.common.utils

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import app.keskonfe.android.ui.theme.PrimaryColor

@Composable
fun LoadingScreen(
    isLoading: Boolean,
    isError: Boolean = false,
    content: @Composable () -> Unit,
    errorContent: @Composable () -> Unit = { }
) = if (isLoading) {
    Box(modifier = Modifier.fillMaxSize()) {
        CircularProgressIndicator(
            color = PrimaryColor,
            modifier = Modifier
                .size(64.dp)
                .align(Alignment.Center)
        )
    }
} else if (isError) {
    errorContent()
} else {
    content()
}