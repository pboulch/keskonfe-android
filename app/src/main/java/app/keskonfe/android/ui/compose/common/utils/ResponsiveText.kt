package app.keskonfe.android.ui.compose.common.utils

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow

private const val TEXT_SCALE_REDUCTION_INTERVAL = 0.9f

@Composable
fun ResponsiveText(
    modifier: Modifier = Modifier,
    text: String,
    textAlign: TextAlign = TextAlign.Center,
    textStyle: TextStyle,
    maxLines: Int = 1,
) {
    var style by remember(textStyle) {
        mutableStateOf(textStyle)
    }

    Text(
        modifier = modifier,
        text = text,
        textAlign = textAlign,
        style = style,
        maxLines = maxLines,
        overflow = TextOverflow.Ellipsis,
        onTextLayout = { textLayoutResult ->
            val maxCurrentLineIndex: Int = textLayoutResult.lineCount - 1

            if (textLayoutResult.isLineEllipsized(maxCurrentLineIndex)) {
                style = style.copy(
                    fontSize = style.fontSize.times(TEXT_SCALE_REDUCTION_INTERVAL)
                )
            }
        },
    )
}