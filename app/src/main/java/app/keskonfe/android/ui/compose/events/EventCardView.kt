package app.keskonfe.android.ui.compose.events

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.keskonfe.android.R
import app.keskonfe.android.extensions.silentClickable
import app.keskonfe.android.models.Event
import app.keskonfe.android.ui.compose.common.ktag.KTag
import app.keskonfe.android.ui.compose.common.ktag.KTagStyle
import app.keskonfe.android.ui.theme.NotoFamily
import app.keskonfe.android.ui.theme.OverlayBlackColor
import app.keskonfe.android.ui.theme.PrimaryColorDark
import coil.compose.AsyncImage
import java.text.DateFormatSymbols


@Composable
fun EventCard(
    event: Event,
    onTap: () -> Unit = { }
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 4.dp)
            .silentClickable {
                onTap()
            },
        shape = RoundedCornerShape(8.dp),
        elevation = 10.dp
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
        ) {
            EventCardHeader(event)
            Column(Modifier.padding(horizontal = 16.dp, vertical = 16.dp)) {
                Text(
                    text = event.title,
                    style = TextStyle(
                        fontFamily = NotoFamily,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 18.sp
                    )
                )
                Text(
                    text = "${event.organizer.name} · ${event.physicalAddress.locality}",
                    style = TextStyle(
                        fontFamily = NotoFamily,
                        fontWeight = FontWeight.Normal,
                        fontSize = 14.sp
                    )
                )
                //EventCardTags(event)
            }
        }
    }
}

@Composable
fun EventCardHeader(event: Event) {

    val month = DateFormatSymbols().months[event.beginsOn.monthValue - 1]
    val monthString = if (month.length > 3) month.substring(0, 4).uppercase() else month.uppercase()

    Box {
        if (event.picture.isNotEmpty()) {
            AsyncImage(
                model = event.picture,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.height(120.dp),
                placeholder = painterResource(id = R.drawable.cover_default)
            )

            Box(
                modifier = Modifier
                    .matchParentSize()
                    .background(OverlayBlackColor)
            )
        } else {
            Image(
                painter = painterResource(id = R.drawable.cover_default),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.height(80.dp)
            )

            Box(
                modifier = Modifier
                    .matchParentSize()
                    .background(OverlayBlackColor)
            )
        }

        Row(
            modifier = Modifier
                .fillMaxSize()
                .height(120.dp)
                .padding(horizontal = 16.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Start,
        ) {
            Column(
                modifier = Modifier
                    .size(50.dp)
                    .clip(RoundedCornerShape(8.dp))
                    .background(color = Color.White.copy(0.8f)),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Box(modifier = Modifier
                    .background(color = PrimaryColorDark)
                    .fillMaxWidth()
                    .height(10.dp))
                Column(modifier = Modifier.fillMaxSize(), verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally) {
                    Text(
                        text = "${event.beginsOn.dayOfMonth}",
                        style = TextStyle(color = PrimaryColorDark, fontWeight = FontWeight.SemiBold)
                    )
                    Text(text = monthString, style = TextStyle(color = PrimaryColorDark, fontWeight = FontWeight.SemiBold))
                }

            }
        }
    }
}

@Composable
fun EventCardTags(event: Event) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp),
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        KTag(
            content = stringResource(id = event.category.labelId),
            style = KTagStyle.CARD,
            isSelected = false
        )
        if (event.organizer.avatar.isNotEmpty()) {
            AsyncImage(
                model = event.organizer.avatar,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(32.dp)
                    .clip(shape = CircleShape)
            )
        }

    }
}