package app.keskonfe.android.ui.compose.events

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import app.keskonfe.android.models.Event

@Composable
fun ListEvent(listState: LazyListState, listEvents: List<Event>) {
    LazyColumn(
        state = listState,
        modifier = Modifier
            .fillMaxSize()
            .background(Color.White),
        contentPadding = PaddingValues(
            start = 16.dp,
            end = 16.dp,
            bottom = 32.dp,
            top = 16.dp
        ),
        verticalArrangement = Arrangement.spacedBy(20.dp),
    ) {

        item {
            //MainFeedHeader()
        }

        items(listEvents) {
            EventCard(
                event = it
            )
        }

    }
}