package app.keskonfe.android.ui.compose.events.detail

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.unit.dp
import app.keskonfe.android.ui.theme.OpenSansBoldText14
import app.keskonfe.android.ui.theme.OpenSansText12
import app.keskonfe.android.ui.theme.PrimaryColorDark
import coil.compose.AsyncImage

@Composable
fun EventDetailInfoItem(
    icon: Painter? = null,
    profilePicture: String? = null,
    textHeader: String,
    textDescription: String
) {
    Row(horizontalArrangement = Arrangement.spacedBy(16.dp)) {
        if (icon != null) {
            Box(
                modifier = Modifier
                    .background(color = Color.White, shape = RoundedCornerShape(8.dp))
                    .size(40.dp)
            ) {
                Icon(
                    painter = icon,
                    contentDescription = null,
                    tint = PrimaryColorDark,
                    modifier = Modifier
                        .height(32.dp)
                        .align(Alignment.Center)
                )
            }
        } else {
            AsyncImage(
                model = profilePicture,
                contentDescription = null,
                modifier = Modifier
                    .size(40.dp)
                    .clip(RoundedCornerShape(8.dp))
            )
        }

        Column(
            verticalArrangement = Arrangement.spacedBy(4.dp),
            modifier = Modifier.height(40.dp)
        ) {
            Text(text = textHeader, style = OpenSansBoldText14)
            Text(text = textDescription, style = OpenSansText12)
        }
    }
}