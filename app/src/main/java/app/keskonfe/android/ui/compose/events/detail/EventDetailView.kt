package app.keskonfe.android.ui.compose.events.detail

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInRoot
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import app.keskonfe.android.R
import app.keskonfe.android.extensions.silentClickable
import app.keskonfe.android.ui.compose.common.buttons.KButton
import app.keskonfe.android.ui.compose.common.buttons.KButtonStyle
import app.keskonfe.android.ui.compose.common.utils.getLocale
import app.keskonfe.android.ui.compose.destinations.KWebViewDestination
import app.keskonfe.android.ui.compose.navigation.HomeNavGraph
import app.keskonfe.android.ui.theme.ColorFive
import app.keskonfe.android.ui.theme.NotoSemiBoldText18
import app.keskonfe.android.ui.theme.OpenSansBoldText16
import app.keskonfe.android.ui.theme.OpenSansFamily
import app.keskonfe.android.vm.events.EventDetailViewModel
import coil.compose.AsyncImage
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import de.charlex.compose.HtmlText
import org.koin.androidx.compose.getViewModel
import org.koin.core.parameter.parametersOf
import java.util.UUID

@HomeNavGraph
@Destination
@Composable
fun EventDetail(
    idEvent: UUID,
    navigator: DestinationsNavigator
) {

    val viewModel = getViewModel<EventDetailViewModel>(parameters = { parametersOf(idEvent) })
    val event by viewModel.event.collectAsState()

    val scrollState = rememberScrollState()
    var showTopBar by rememberSaveable { mutableStateOf(false) }

    if (viewModel.isLoading) {
        Box(
            modifier = Modifier
                .fillMaxSize()

                .background(Color.White)
        ) {
            Box(
                modifier = Modifier
                    .size(100.dp)
                    .align(Alignment.Center)
            ) {
                CircularProgressIndicator(color = ColorFive, modifier = Modifier.size(100.dp))
            }
        }
    } else {
        event?.let { currentEvent ->
            Box(modifier = Modifier
                .fillMaxSize()
                .navigationBarsPadding()) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(Color.White)
                        .verticalScroll(scrollState)
                ) {
                    ConstraintLayout(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(200.dp)
                    ) {
                        val (backButton, backgroundImage) = createRefs()

                        if (currentEvent.picture.isNotEmpty()) {
                            AsyncImage(
                                model = currentEvent.picture,
                                contentDescription = null,
                                contentScale = ContentScale.Crop,
                                modifier = Modifier
                                    .height(200.dp)
                                    .constrainAs(backgroundImage) {
                                        top.linkTo(parent.top)
                                        start.linkTo(parent.start)
                                        bottom.linkTo(parent.bottom)
                                        end.linkTo(parent.end)
                                    },
                                placeholder = painterResource(id = R.drawable.cover_default)
                            )
                        } else {
                            Image(
                                painter = painterResource(id = R.drawable.cover_default),
                                contentDescription = null,
                                contentScale = ContentScale.Crop,
                                modifier = Modifier
                                    .height(200.dp)
                                    .constrainAs(backgroundImage) {
                                        top.linkTo(parent.top)
                                        start.linkTo(parent.start)
                                        bottom.linkTo(parent.bottom)
                                        end.linkTo(parent.end)
                                    }
                            )
                        }

                        Box(modifier = Modifier
                            .size(32.dp)
                            .background(color = Color.White, shape = CircleShape)
                            .constrainAs(backButton) {
                                top.linkTo(parent.top, margin = 8.dp)
                                start.linkTo(parent.start, margin = 8.dp)
                            }
                            .silentClickable {
                                navigator.popBackStack()
                            },
                            contentAlignment = Alignment.Center
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_arrow_back),
                                contentDescription = "Close button",
                                tint = Color.Black,
                                modifier = Modifier.size(24.dp)
                            )
                        }

                    }
                    Spacer(
                        modifier = Modifier
                            .height(24.dp)
                            .onGloballyPositioned { coordinates ->
                                showTopBar = coordinates.positionInRoot().y.toInt() <= 0
                            }
                    )
                    Column(
                        modifier = Modifier
                            .padding(horizontal = 16.dp)
                            .padding(bottom = 48.dp),
                        verticalArrangement = Arrangement.spacedBy(16.dp)
                    ) {
                        Text(
                            text = currentEvent.title,
                            style = NotoSemiBoldText18
                        )
                        EventDetailInfoItem(
                            icon = painterResource(id = R.drawable.ic_calendar_bis),
                            textHeader = "${currentEvent.beginsOn.dayOfMonth} ${
                                currentEvent.beginsOn.month.getDisplayName(
                                    java.time.format.TextStyle.FULL,
                                    getLocale()
                                )
                            } ${currentEvent.beginsOn.year}",
                            textDescription = "${currentEvent.beginsOn.hour} h ${if(currentEvent.beginsOn.minute > 0) {currentEvent.beginsOn.minute} else {""}}"
                        )
                        EventDetailInfoItem(
                            icon = painterResource(id = R.drawable.ic_location),
                            textHeader = if(currentEvent.physicalAddress.description != currentEvent.physicalAddress.street) { currentEvent.physicalAddress.description } else {currentEvent.physicalAddress.locality},
                            textDescription = currentEvent.physicalAddress.street+" • "+currentEvent.physicalAddress.locality
                        )
                        if (currentEvent.organizer.name.isNotEmpty()) {
                            EventDetailInfoItem(
                                profilePicture = currentEvent.organizer.avatar,
                                textHeader = currentEvent.organizer.name,
                                textDescription = "Organisateur"
                            )
                        }

                        Text(
                            text = stringResource(id = R.string.event_detail_description),
                            style = OpenSansBoldText16
                        )
                        if (currentEvent.description.isNotEmpty()) {
                            HtmlText(
                                text = currentEvent.description,
                                fontFamily = OpenSansFamily,
                                fontSize = 14.sp,
                                onUriClick = {
                                    navigator.navigate(
                                        KWebViewDestination(
                                            url = it,
                                            title = currentEvent.title
                                        )
                                    )
                                },
                                urlSpanStyle = SpanStyle(
                                    color = ColorFive,
                                    textDecoration = TextDecoration.Underline
                                )
                            )
                        }
                    }
                }
                if (currentEvent.externalParticipationUrl != null) {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .align(Alignment.BottomCenter)
                            .padding(16.dp)
                    ) {

                        KButton(
                            text = stringResource(id = R.string.event_detail_cta),
                            style = KButtonStyle.XL_FIVE,
                            isLoading = false,
                            onClick = {
                                navigator.navigate(
                                    KWebViewDestination(
                                        url = currentEvent.externalParticipationUrl!!,
                                        title = currentEvent.title
                                    )
                                )
                            },
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                }
            }
        }
    }

}