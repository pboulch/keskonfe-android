package app.keskonfe.android.ui.compose.mainfeed

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.capitalize
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.keskonfe.android.R
import app.keskonfe.android.extensions.silentClickable
import app.keskonfe.android.ui.compose.common.fields.KTextBox
import app.keskonfe.android.ui.theme.NotoFamily
import app.keskonfe.android.ui.theme.PrimaryColor
import app.keskonfe.android.ui.theme.PrimaryColorDark

@Composable
fun MainFeedHeader(
    onSearchChange: (value: String?) -> Unit = { },
    onSearch: () -> Unit = {},
    searchQuery: String?,
    searchFieldFocusRequester: FocusRequester = FocusRequester(),
    city: String
) {

    val focusManager = LocalFocusManager.current

    var tempQuery by remember { mutableStateOf("") }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                brush = Brush.horizontalGradient(
                    colors = listOf(
                        PrimaryColor,
                        PrimaryColorDark
                    )
                )
            ),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(16.dp))
        Row(
            horizontalArrangement = Arrangement.spacedBy(4.dp),
            verticalAlignment = Alignment.CenterVertically) {
                Text(
                    text = stringResource(id = R.string.feed_header_location) + " " + city.lowercase().replaceFirstChar(Char::titlecase),
                    style = TextStyle(fontFamily = NotoFamily, fontSize = 14.sp, color = Color.White),
                )
                Icon(
                    painter = painterResource(id = R.drawable.ic_half_arrow_down),
                    contentDescription = null,
                    tint = Color.White
                )
        }
        

        Box(
            Modifier
                .padding(horizontal = 16.dp)
                .padding(top = 8.dp, bottom = 16.dp)
        ) {
            KTextBox(
                value = searchQuery ?: "",
                hint = stringResource(id = R.string.feed_header_toulouse_hint),
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Search
                ),
                onValueChange = {
                    tempQuery = it ?: ""
                    if (it.isNullOrEmpty()) {
                        onSearchChange(null)
                    } else {
                        onSearchChange(tempQuery)
                    }
                },
                keyboardActions = KeyboardActions(
                    onSearch = {
                        onSearch()
                        focusManager.clearFocus()
                    }
                ),
                focusRequester = searchFieldFocusRequester
            )
        }

    }
}