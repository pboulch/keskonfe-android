package app.keskonfe.android.ui.compose.mainfeed

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.keskonfe.android.R
import app.keskonfe.android.extensions.OnBottomReached
import app.keskonfe.android.extensions.convertPixelsToDp
import app.keskonfe.android.extensions.detectTapAndPressUnconsumed
import app.keskonfe.android.models.City
import app.keskonfe.android.models.Event
import app.keskonfe.android.models.ListDate
import app.keskonfe.android.ui.compose.common.buttons.KButton
import app.keskonfe.android.ui.compose.common.buttons.KButtonStyle
import app.keskonfe.android.ui.compose.common.loaders.Loader
import app.keskonfe.android.ui.compose.common.loaders.NotFound
import app.keskonfe.android.ui.compose.destinations.EventDetailDestination
import app.keskonfe.android.ui.compose.destinations.SelectLocationSheetDestination
import app.keskonfe.android.ui.compose.events.EventCard
import app.keskonfe.android.ui.compose.navigation.HomeNavGraph
import app.keskonfe.android.ui.theme.ColorFive
import app.keskonfe.android.ui.theme.NotoFamily
import app.keskonfe.android.ui.theme.NotoSemiBoldFive18
import app.keskonfe.android.vm.mainfeed.MainFeedViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import org.koin.androidx.compose.getViewModel


@HomeNavGraph
@Destination
@Composable
fun MainFeed(
    navigator: DestinationsNavigator
) {
    val viewModel = getViewModel<MainFeedViewModel>()
    val mainFeed by viewModel.mainFeed.collectAsState()
    val context = LocalContext.current
    val configuration = LocalConfiguration.current
    val screenWidth = configuration.screenWidthDp.dp

    val isLoading by viewModel.isLoading.collectAsState()
    val cityName by viewModel.city.collectAsState(initial = City.TOULOUSE)

    val listState = rememberLazyListState()
    val searchFieldFocusRequester = remember { FocusRequester() }
    val focusManager = LocalFocusManager.current

    val query by viewModel.query.collectAsState()

    Box(modifier = Modifier
        .fillMaxSize()
        .pointerInput(Unit) {
            detectTapAndPressUnconsumed(onTap = {
                if (convertPixelsToDp(it.y, context) < 100 && convertPixelsToDp(
                        it.y,
                        context
                    ) > 50
                ) {
                    if (convertPixelsToDp(
                            it.x,
                            context
                        ).dp < (screenWidth - 16.dp) && convertPixelsToDp(
                            it.x,
                            context
                        ).dp > (screenWidth - 50.dp)
                    ) {
                        viewModel.clearSearch()
                    } else if (convertPixelsToDp(
                            it.x,
                            context
                        ).dp < (screenWidth - 50.dp) && convertPixelsToDp(
                            it.x,
                            context
                        ).dp > (screenWidth - 96.dp)
                    ) {
                        viewModel.search()
                        focusManager.clearFocus()
                    } else {
                        searchFieldFocusRequester.requestFocus()
                    }
                } else {
                    focusManager.clearFocus()
                }

                if (convertPixelsToDp(it.y, context) < 50 && convertPixelsToDp(
                        it.y,
                        context
                    ) > 20
                ) {
                    navigator.navigate(SelectLocationSheetDestination)
                }

            })
        }) {

        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 160.dp)
                .background(Color.White)
        ) {}

        MainFeedHeader(
            onSearchChange = {
                viewModel.updateSearch(it)
            },
            onSearch = {
                viewModel.search()
            },
            searchFieldFocusRequester = searchFieldFocusRequester,
            searchQuery = query,
            city = cityName.toString()
        )


        LazyColumn(
            state = listState,
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Transparent),
            contentPadding = PaddingValues(
                top = 128.dp
            ),
        ) {

            item {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(32.dp)
                        .background(
                            Color.White,
                            shape = RoundedCornerShape(topStart = 32.dp, topEnd = 32.dp)
                        )

                ) {
                    if (isLoading && mainFeed.isNotEmpty()) {
                        CircularProgressIndicator(
                            modifier = Modifier
                                .size(24.dp)
                                .align(Alignment.Center), color = ColorFive, strokeWidth = 2.dp
                        )
                    }
                }
            }

            if (mainFeed.isEmpty() && isLoading) {
                item {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .align(Alignment.Center)
                    ) {
                        Column(
                            modifier = Modifier.align(Alignment.Center),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Box(modifier = Modifier.size(200.dp)) {
                                Loader()
                            }
                            Text(text = "Chargement ...", style = NotoSemiBoldFive18)
                        }
                    }

                }
            } else if (mainFeed.isEmpty()) {
                item {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .align(Alignment.Center)
                    ) {
                        Column(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(horizontal = 16.dp)
                                .align(Alignment.Center),
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            Box(modifier = Modifier.size(200.dp)) {
                                NotFound()
                            }
                            Text(
                                text = stringResource(id = R.string.feed_placeholder_empty),
                                style = NotoSemiBoldFive18,
                                textAlign = TextAlign.Center
                            )
                            KButton(
                                text = stringResource(id = R.string.feed_placeholder_cta),
                                style = KButtonStyle.L_FIVE,
                                modifier = Modifier.padding(vertical = 16.dp),
                                onClick = {
                                    viewModel.clearSearch()
                                    viewModel.fetchFeed()
                                }
                            )
                        }
                    }

                }
            }
            mainFeed.entries.forEach { date: Map.Entry<ListDate,List<Event>> ->
                item {
                    Box(modifier = Modifier
                        .fillMaxWidth()
                        .background(Color.White)) {
                        Text(
                            dateListItem(date = date.key),
                            style = TextStyle(
                                fontFamily = NotoFamily,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 16.sp,
                                color = ColorFive
                            ),
                            modifier = Modifier
                                .padding(horizontal = 16.dp)
                                .padding(bottom = 16.dp)
                        )
                    }

                }
                items(date.value) {
                    Box(modifier = Modifier.background(color = Color.White)) {
                        Box(
                            modifier = Modifier
                                .padding(horizontal = 16.dp)
                                .padding(bottom = 16.dp)
                        ) {
                            EventCard(
                                event = it,
                                onTap = {
                                    navigator.navigate(EventDetailDestination(idEvent = it.uuid))
                                })
                        }
                    }
                }
            }
        }
    }



    listState.OnBottomReached(buffer = 1) { viewModel.onLoadMore() }

}

@Composable
private fun dateListItem(date: ListDate): String {
    return when(date) {
        ListDate.TODAY -> stringResource(id = R.string.feed_date_today)
        ListDate.TOMORROW -> stringResource(id = R.string.feed_date_tomorrow)
        ListDate.WEDNESDAY -> stringResource(id = R.string.feed_date_wednesday)
        ListDate.THURSDAY -> stringResource(id = R.string.feed_date_thursday)
        ListDate.FRIDAY -> stringResource(id = R.string.feed_date_friday)
        ListDate.SATURDAY -> stringResource(id = R.string.feed_date_saturday)
        ListDate.SUNDAY -> stringResource(id = R.string.feed_date_sunday)
        ListDate.NEXT_WEEK -> stringResource(id = R.string.feed_date_next_week)
        ListDate.THIS_MONTH -> stringResource(id = R.string.feed_date_this_month)
        else -> stringResource(id = R.string.feed_date_next)
    }
}
