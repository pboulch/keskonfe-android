package app.keskonfe.android.ui.compose.modals

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import app.keskonfe.android.extensions.silentClickable
import app.keskonfe.android.ui.compose.common.buttons.KButton
import app.keskonfe.android.ui.compose.common.ktag.KTagStyle
import app.keskonfe.android.ui.compose.navigation.HomeNavGraph
import app.keskonfe.android.ui.compose.onboarding.OnboardingCitySelect
import app.keskonfe.android.ui.theme.NotoSemiBoldFive18
import app.keskonfe.android.vm.onboarding.OnboardingViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.spec.DestinationStyleBottomSheet
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel
import app.keskonfe.android.R
import app.keskonfe.android.ui.theme.ColorFive


@Destination(style = DestinationStyleBottomSheet::class)
@HomeNavGraph
@Composable
fun SelectLocationSheet(
    navigator: DestinationsNavigator
) {

    val context = LocalContext.current
    val coroutineScope = rememberCoroutineScope()
    val viewModel = getViewModel<OnboardingViewModel>()

    Column {
        OnboardingCitySelect(onTapCity = {
            coroutineScope.launch {
                viewModel.saveCity(it, context)
                navigator.popBackStack()
            }
        }, textStyle = NotoSemiBoldFive18, tagStyle = KTagStyle.SHEET)

        Box(modifier = Modifier.padding(16.dp), contentAlignment = Alignment.Center) {
            Text(
                text = stringResource(id = R.string.feed_privacy),
                style = TextStyle(
                    color = ColorFive,
                    textDecoration = TextDecoration.Underline
                ),
                modifier = Modifier.silentClickable {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.setData(Uri.parse("https://keskonfe.app/privacy.html"))
                    context.startActivity(intent)
                }
            )
        }
    }

}