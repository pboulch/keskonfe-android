package app.keskonfe.android.ui.compose.navigation

import com.ramcosta.composedestinations.annotation.NavGraph

@NavGraph
annotation class HomeNavGraph(
    val start: Boolean = false
)