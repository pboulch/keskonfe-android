package app.keskonfe.android.ui.compose.onboarding

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import app.keskonfe.android.R
import app.keskonfe.android.extensions.conditionalIf
import app.keskonfe.android.extensions.silentClickable
import app.keskonfe.android.models.City
import app.keskonfe.android.ui.compose.common.buttons.KButton
import app.keskonfe.android.ui.compose.common.buttons.KButtonStyle
import app.keskonfe.android.ui.compose.common.ktag.KTag
import app.keskonfe.android.ui.compose.common.ktag.KTagStyle
import app.keskonfe.android.ui.compose.destinations.MainFeedDestination
import app.keskonfe.android.ui.compose.navigation.HomeNavGraph
import app.keskonfe.android.ui.theme.ColorFive
import app.keskonfe.android.ui.theme.NotoSemiBoldText18
import app.keskonfe.android.ui.theme.NotoSemiBoldWhite24
import app.keskonfe.android.ui.theme.OpenSansWhite14
import app.keskonfe.android.vm.onboarding.OnboardingViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.annotation.RootNavGraph
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.compose.getViewModel

@OptIn(ExperimentalLayoutApi::class, ExperimentalFoundationApi::class)
@HomeNavGraph
@Destination
@Composable
fun Onboarding(navigator: DestinationsNavigator) {
    val context = LocalContext.current
    val pagerState = rememberPagerState(pageCount = { 2 })
    val coroutineScope = rememberCoroutineScope()
    val viewModel = getViewModel<OnboardingViewModel>()
    
    HorizontalPager(state = pagerState) { page ->
        Column(Modifier.fillMaxSize()) {
            when(page) {
                0 -> OnboardingIntroduction(onTapToNext = {
                    coroutineScope.launch {
                        pagerState.scrollToPage(1)
                    }
                })
                1 -> OnboardingCitySelect(onTapCity = { city ->
                    coroutineScope.launch {
                        viewModel.saveCity(city, context)
                        navigator.navigate(MainFeedDestination)
                    }
                })
            }

        }

    }
}

@Composable
private fun OnboardingIntroduction(
    onTapToNext: () -> Unit
) {
    val context = LocalContext.current
    Column(modifier = Modifier.padding(24.dp), verticalArrangement = Arrangement.SpaceBetween) {
        Column(modifier = Modifier
            .fillMaxSize().weight(1f), verticalArrangement = Arrangement.Center) {
            Text(text = stringResource(id = R.string.onboarding_intro_title),
                style = NotoSemiBoldWhite24,
                modifier = Modifier.padding(bottom = 24.dp))
            Text(text = stringResource(id = R.string.onboarding_intro_content),
                style = OpenSansWhite14
            )
            Row(modifier = Modifier
                .fillMaxWidth()
                .padding(top = 24.dp), horizontalArrangement = Arrangement.End) {
                KButton(
                    text = stringResource(id = R.string.onboarding_intro_cta),
                    style = KButtonStyle.XL_TRANSPARENT,
                    onClick = {
                        onTapToNext()
                    }
                )
            }
        }
        Box(modifier = Modifier.padding(16.dp).fillMaxWidth(), contentAlignment = Alignment.CenterEnd) {
            Text(
                text = stringResource(id = R.string.feed_privacy),
                style = TextStyle(
                    color = Color.White,
                    textDecoration = TextDecoration.Underline
                ),
                modifier = Modifier.silentClickable {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.setData(Uri.parse("https://keskonfe.app/privacy.html"))
                    context.startActivity(intent)
                }
            )
        }
    }

}

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun OnboardingCitySelect(
    onTapCity: (City) -> Unit,
    textStyle: TextStyle = NotoSemiBoldWhite24,
    tagStyle: KTagStyle = KTagStyle.ONBOARDING
) {
    Column(
        modifier = Modifier
            .padding(24.dp)
            .conditionalIf(tagStyle != KTagStyle.SHEET) { fillMaxSize() }
        , verticalArrangement = Arrangement.Center
    ) {
        Text(
            stringResource(id = R.string.onboarding_city_title),
            style = textStyle,
            modifier = Modifier.padding(bottom = 24.dp)
        )
        FlowRow {
            KTag(
                content = stringResource(id = R.string.onboarding_city_rennes),
                style = tagStyle,
                isSelected = false,
                onTap = { onTapCity(City.RENNES) }
            )
            KTag(
                content = stringResource(id = R.string.onboarding_city_toulouse),
                style = tagStyle,
                isSelected = false,
                onTap = { onTapCity(City.TOULOUSE) }
            )
        }
    }
}