package app.keskonfe.android.ui.compose.startup

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import app.keskonfe.android.ui.compose.destinations.MainFeedDestination
import app.keskonfe.android.ui.compose.destinations.OnboardingDestination
import app.keskonfe.android.ui.compose.navigation.HomeNavGraph
import app.keskonfe.android.vm.onboarding.OnboardingViewModel
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import org.koin.androidx.compose.getViewModel
import app.keskonfe.android.R

@HomeNavGraph(start = true)
@Destination
@Composable
fun Startup(
    navigator: DestinationsNavigator
) {

    val viewModel = getViewModel<OnboardingViewModel>()

    LaunchedEffect(key1 = Unit) {
        viewModel.city.collect {
            if(it == null) {
                navigator.navigate(OnboardingDestination)
            } else {
                navigator.navigate(MainFeedDestination)
            }
        }
    }

    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Image(
            painter = painterResource(id = R.mipmap.ic_launcher_foreground),
            contentDescription = null,
            modifier = Modifier.size(200.dp)
        )
    }

}