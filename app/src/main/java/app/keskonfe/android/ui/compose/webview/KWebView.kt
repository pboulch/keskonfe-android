package app.keskonfe.android.ui.compose.webview

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import app.keskonfe.android.R
import app.keskonfe.android.ui.compose.common.topbar.TopBarView
import app.keskonfe.android.ui.compose.navigation.HomeNavGraph
import app.keskonfe.android.ui.theme.NotoSemiBoldText18
import app.keskonfe.android.ui.theme.PrimaryColor
import app.keskonfe.android.ui.theme.TextColor
import com.google.accompanist.web.AccompanistWebViewClient
import com.google.accompanist.web.WebView
import com.google.accompanist.web.rememberWebViewState
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.spec.DestinationStyleBottomSheet
import timber.log.Timber

@SuppressLint("SetJavaScriptEnabled")
@HomeNavGraph
@Destination(style = DestinationStyleBottomSheet::class)
@Composable
fun KWebView(
    rootNavigator: DestinationsNavigator,
    title: String,
    url: String
) {
    val state =
        rememberWebViewState(url = url.replace("http:", "https:").replace("hellowork:", "https:"))

    val webClient = remember {
        object : AccompanistWebViewClient() {
            override fun onPageStarted(
                view: WebView,
                url: String?,
                favicon: Bitmap?
            ) {
                super.onPageStarted(view, url, favicon)
                Timber.d("Page started loading for $url")
            }

            override fun onReceivedHttpError(
                view: WebView?,
                request: WebResourceRequest?,
                errorResponse: WebResourceResponse?
            ) {
                super.onReceivedHttpError(view, request, errorResponse)
            }
        }
    }

    Column(
        modifier = Modifier
            .systemBarsPadding()
            .imePadding()
            .fillMaxHeight(0.96f)
    ) {
        TopBarView(
            viewCenter = {
                Text(stringResource(id = R.string.app_name), style = NotoSemiBoldText18)
            },
            viewLeft = {
                IconButton(onClick = {
                    rootNavigator.popBackStack()
                }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_arrow_back),
                        contentDescription = stringResource(id = R.string.common_back),
                        tint = TextColor
                    )
                }
            },
            viewRight = {
                if (state.isLoading) {
                    CircularProgressIndicator(
                        color = PrimaryColor,
                        strokeWidth = 3.dp,
                        modifier = Modifier
                            .padding(end = 16.dp)
                            .size(20.dp)
                    )
                }
            },
            withDivider = true
        )
        WebView(
            state = state,
            modifier = Modifier
                .weight(1f)
                .verticalScroll(rememberScrollState()),
            onCreated = { webView ->
                webView.settings.javaScriptEnabled = true
            },
            client = webClient
        )
    }

}