package app.keskonfe.android.ui.theme

import androidx.compose.ui.graphics.Color

val PrimaryColorDark = Color(0xFFB61624)
val PrimaryColor = Color(0xFFe63946)
val PrimaryColorLight = Color(0xFFEB5C68)
val PrimaryColorLighter = Color(0xFFEF8089)
val PrimaryColorLightest = Color(0x88F6B6BC)
var ColorTwo = Color(0xFFf1faee)
var ColorThree = Color(0xFFa8dadc)
var ColorFour = Color(0xFF457b9d)
var ColorFive = Color(0xFF1d3557)

var TagColorOne = Color(0xFF59cd90)
var TagColorTwo = Color(0xFF3fa7d6)
var TagColorThree = Color(0xFFfac05e)
var TagColorFour = Color(0xFFf79d84)
var TagColorFive = Color(0xFF59cd90)

val TextColor = Color(0xFF303030)
val SecondaryTextColor = Color(0xFF222222)
val SubTextColor = Color(0xFF737373)
val GreyBorderColor = Color(0xFFCCCCCC)
val GreyBorderColorLight = Color(0xFFEFEFEF)

val GreyColor = Color(0xFF606060)
val GreyLightColor = Color(0xFFADADAD)
val GreyLighterColor = Color(0xFFE5E5E5)
val GreyLightestColor = Color(0xFFF6F6F6)

val OverlayBlackColor = Color(0x44303030)
val GradientEnd = Color(0xFF886BF1)
val GradientStart = Color(0xFFAD98FA)