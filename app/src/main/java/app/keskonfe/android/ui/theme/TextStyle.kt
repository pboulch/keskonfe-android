package app.keskonfe.android.ui.theme

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

val OpenSansText16 = TextStyle(
    color = TextColor,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 16.sp
)

val OpenSansPrimaryDark16 = TextStyle(
    color = PrimaryColorDark,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 16.sp
)

val OpenSansBoldText16 = TextStyle(
    color = TextColor,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Bold,
    fontSize = 16.sp
)

val OpenSansText14 = TextStyle(
    color = TextColor,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp
)

val OpenSansWhite14 = TextStyle(
    color = Color.White,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp
)

val OpenSansBoldText14 = TextStyle(
    color = TextColor,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Bold,
    fontSize = 14.sp
)

val OpenSansSubText14 = TextStyle(
    color = SubTextColor,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp
)

val OpenSansSecondaryText14 = TextStyle(
    color = SecondaryTextColor,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp
)

val OpenSansPrimaryDark14 = TextStyle(
    color = PrimaryColorDark,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 14.sp
)

val OpenSansText12 = TextStyle(
    color = TextColor,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 12.sp
)

val OpenSansSubText12 = TextStyle(
    color = SubTextColor,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 12.sp
)

val OpenSansWhite12 = TextStyle(
    color = Color.White,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 12.sp
)

val OpenSansPrimaryDark12 = TextStyle(
    color = PrimaryColorDark,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 12.sp
)

val OpenSansClear12 = TextStyle(
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 12.sp
)

val OpenSansBoldText12 = TextStyle(
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Bold,
    fontSize = 12.sp,
    color = TextColor
)

val OpenSansBoldPrimaryDark14 = TextStyle(
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Bold,
    fontSize = 14.sp,
    color = PrimaryColorDark
)

val OpenSansWhite10 = TextStyle(
    color = Color.White,
    fontFamily = OpenSansFamily,
    fontWeight = FontWeight.Normal,
    fontSize = 10.sp
)

/***
 * NOTO STYLE
 */

val NotoSemiBoldText18 = TextStyle(
    color = TextColor,
    fontFamily = NotoFamily,
    fontWeight = FontWeight.SemiBold,
    fontSize = 18.sp
)

val NotoSemiBoldWhite24 = TextStyle(
    color = Color.White,
    fontFamily = NotoFamily,
    fontWeight = FontWeight.SemiBold,
    fontSize = 24.sp
)

val NotoSemiBoldFive18 = TextStyle(
    color = ColorFive,
    fontFamily = NotoFamily,
    fontWeight = FontWeight.SemiBold,
    fontSize = 18.sp
)

