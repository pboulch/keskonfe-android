package app.keskonfe.android.utils.compose

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.google.accompanist.navigation.material.BottomSheetNavigator
import com.google.accompanist.navigation.material.ExperimentalMaterialNavigationApi

@Stable
class KBottomSheetHostState {
    var dismissEnabled by mutableStateOf(true)

    /**
     * Désactive le "swipeToDismiss" d'une sheet affichée via notre BottomSheetNavigator tant que ce composable est dans la composition.
     * Attention à bien appeler enableSwipeToDismiss avant de fermer la sheet.
     * @see enableSwipeToDismiss
     */
    @Composable
    fun disableSwipeToDismissInComposition() {
        LaunchedEffect(Unit) {
            dismissEnabled = false
        }
    }

    fun enableSwipeToDismiss() {
        dismissEnabled = true
    }
}

@OptIn(ExperimentalMaterialApi::class, ExperimentalMaterialNavigationApi::class)
@Composable
fun rememberKBottomSheetNavigator(state: KBottomSheetHostState): BottomSheetNavigator {
    val skipHalfExpanded = true

    val sheetState = rememberModalBottomSheetState(
        initialValue = ModalBottomSheetValue.Hidden,
        confirmValueChange = {
            if (state.dismissEnabled) {
                true
            } else {
                it != ModalBottomSheetValue.Hidden
            }
        },
        skipHalfExpanded = skipHalfExpanded
    )

    return remember { BottomSheetNavigator(sheetState) }
}