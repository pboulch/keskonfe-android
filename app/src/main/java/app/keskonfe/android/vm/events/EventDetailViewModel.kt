package app.keskonfe.android.vm.events

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.keskonfe.android.models.Event
import app.keskonfe.android.repository.EventRepository
import kotlinx.coroutines.launch
import java.util.UUID

class EventDetailViewModel(
    private val eventId: UUID,
    private val eventRepository: EventRepository
) : ViewModel() {

    var event = eventRepository.getCurrentEvent()
        private set
    var isLoading by mutableStateOf(false)
        private set

    init {
        isLoading = true
        viewModelScope.launch {
            eventRepository.fetchEvent(eventId)
            isLoading = false
        }
    }

    fun getEvent(): Event? {
        val event = eventRepository.getCurrentEvent().value
        return event
    }

}