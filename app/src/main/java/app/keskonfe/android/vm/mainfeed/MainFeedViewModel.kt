package app.keskonfe.android.vm.mainfeed

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.keskonfe.android.data.FeedDataStore
import app.keskonfe.android.repository.FeedRepository
import kotlinx.coroutines.launch

class MainFeedViewModel(
    private val feedRepository: FeedRepository,
    private val feedDataStore: FeedDataStore
) : ViewModel() {

    var query = feedRepository.getCurrentSearch()
    var mainFeed = feedRepository.getMainFeed()
    var isLoading = feedRepository.isLoading()
    var city = feedDataStore.cityNameFlow

    fun fetchFeed() {
        viewModelScope.launch {
            feedRepository.fetchFeed()
        }
    }

    fun updateSearch(search: String?) {
        viewModelScope.launch {
            feedRepository.updateSearch(search)
        }
    }

    fun search() {
        viewModelScope.launch {
            feedRepository.fetchFeed()
        }
    }

    fun onLoadMore() {
        viewModelScope.launch {
            feedRepository.loadMore()
        }
    }

    fun clearSearch() {
        viewModelScope.launch {
            feedRepository.updateSearch(null)
            feedRepository.fetchFeed()
        }
    }
}

