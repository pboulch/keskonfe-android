package app.keskonfe.android.vm.onboarding

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.keskonfe.android.data.FeedDataStore
import app.keskonfe.android.data.dataStore
import app.keskonfe.android.models.City
import app.keskonfe.android.repository.FeedRepository
import kotlinx.coroutines.launch

class OnboardingViewModel(
    private val feedRepository: FeedRepository
) : ViewModel() {

    var city = feedRepository.getCity()

    suspend fun saveCity(city: City, context: Context) {
        feedRepository.saveCityToFeedDataStore(city, context)
    }
}

